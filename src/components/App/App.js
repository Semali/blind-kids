import React, { Component } from 'react';
import Main from '../../pages/Main'

export class App extends Component {
  render() {
    return (
      <div className="App">
        <Main/>
        <div className="credits">
          <span>Приложение сделал: <a href="mailto:semali1989@gmail.com">Фрейдлин Сергей</a> / </span>
          <span>Дизайн: <a href="mailto:xotena@gmail.com">Фрейдлин Александра</a></span>
        </div>
      </div>
    );
  }
}
