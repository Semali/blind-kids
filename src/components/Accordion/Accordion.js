import React, { Component } from 'react';

export class Accordion extends Component {
  render() {
    const { title, expand, onClick, children } = this.props;

    return (
      <div className="accordion">
        <dt className={expand ? 'accordion__title is-expanded' : 'accordion__title'} onClick={onClick}>
          {title}
        </dt>
        <dd className={expand ? 'accordion__content is-expanded' : 'accordion__content'}>
          <div>
            {children}
          </div>
        </dd>
      </div>
    );

  }
}
