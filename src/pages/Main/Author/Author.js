import React from 'react';
import authorSign from '../../../assets/images/author-sign.svg';
import authorSignActive from '../../../assets/images/author-sign-active.svg';
import Accordion from "../../../components/Accordion";

const block = "author";

export class Author extends React.PureComponent {
  render() {
    const { isColored, show, author, authorTitle, playList } = this.props;
    const wb = isColored ? '' : ' wb';
    const title = <>
      <img className="pen" src={show ? authorSign : authorSignActive} alt=""/>
      <div className={block + "__title" + wb}>{authorTitle}</div>
    </>;

    return (
      <div className={block + wb}>
        <Accordion
          title={title}
          onClick={this.onClick.bind(this)}
          expand={show}
        >
          {
            playList.map((elem, i) => {
              return (
                <div className={block + "__sing-box"} key={i}>
                  <h3 className={block + "__title-sing" + wb}>
                    {elem.title}
                  </h3>
                  <div className={block + "__reader" + wb}>
                    Читает: {elem.reader}
                  </div>
                  <div className={block + "__place" + wb}>
                    {elem.place}
                  </div>
                  <div>
                    <audio
                      className={block + "__audio " + author}
                      controls="controls"
                      onPlaying={this.playAudio.bind(this)}
                      src={elem.file}
                    >
                      Ваш браузер не поддерживает <code>audio</code> элемент.
                    </audio>
                  </div>
                </div>
              );
            })
          }
        </Accordion>
      </div>
    );
  }

  onClick() {
    const { onClick, author } = this.props;

    onClick(author);
  }

  playAudio(event) {
    const audio = document.querySelectorAll('audio');

    for (let key in audio) {
      if (typeof audio[key] === 'object') {
        if (audio[key].src !== event.target.src) {
          audio[key].pause();
        }
      }
    }
  }
}
