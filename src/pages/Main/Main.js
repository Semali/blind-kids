import React, { Component } from 'react';
import vision from '../../assets/images/vision.svg';
import kidsColor from '../../assets/images/kids-color.svg';
import kidsWB from '../../assets/images/kids-wb.svg';
import { playList } from "../../data/playList";
import Author from "./Author";

export class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isColored: true,
    };

    this.changeColor = this.changeColor.bind(this);
    this.toggleBlock = this.toggleBlock.bind(this);
  }

  componentDidMount() {
    const authors = {};

    playList.forEach(elem => {
      authors[elem.author] = false;

      elem.items.sort((a, b) => {
        if (a.order && b.order) {
          return (a.order > b.order) ? 1 : (a.order < b.order) ? -1 : 0;
        }
        if (a.order && !b.order) {
          return -1;
        }
        if (!a.order && b.order) {
          return 1;
        }
        return (a.title.toLowerCase() > b.title.toLowerCase()) ? 1 : (a.title.toLowerCase() < b.title.toLowerCase()) ? -1 : 0;
      });
    });

    this.setState(authors);
  }

  render() {
    const {
      isColored,
    } = this.state;
    const wb = isColored ? '' : ' wb';

    return (
      <div className={"main" + wb}>
        <div className="main__header">
          <div>
            <h1 className={"main__header_title" + wb}>Дети читают детям</h1>
            <span className={"main__header_subtitle" + wb}>Выберите автора, а после - произведение, чтобы его прослушать</span>
          </div>
          <div>
            <button className="main__btn" onClick={this.changeColor}>
              <img className="main__img-vision" src={vision} alt="Версия для слабовидящих"/>
              <div className="main__btn-text">
                {
                  isColored ?
                    'Версия для слабовидящих' :
                    'К стандартной версии'
                }
              </div>
            </button>
          </div>
        </div>
        <div className="main__content">
          <div>
            {
              playList.map(elem => {
                return (
                  <Author
                    key={elem.author}
                    show={this.state[elem.author]}
                    author={elem.author}
                    authorTitle={elem.title}
                    playList={elem.items}
                    isColored={isColored}
                    onClick={this.toggleBlock}
                  />
                );
              })
            }
          </div>
          <div>
            <img className="main__background-img" src={isColored ? kidsColor : kidsWB} alt="kids"/>
          </div>
        </div>
      </div>
    );
  }

  changeColor() {
    const { isColored } = this.state;

    this.setState({
      isColored: !isColored
    });
  }

  toggleBlock(blockName) {
    const authors = {};

    playList.forEach(elem => {
      if (elem.author === blockName) {
        authors[elem.author] = !this.state[blockName];
      } else {
        authors[elem.author] = false;
      }
    });

    this.setState(authors);

    const audio = document.querySelectorAll('audio');

    for (let key in audio) {
      if (typeof audio[key] === 'object') {
        audio[key].pause();
      }
    }
  }
}
